
const arrEx = [12, '12', 'name',  null, 0, true, 34]

filterBy = (arr, type) => {
    return arr.filter((elem) => typeof(elem) !== type);
}


const arrByFilterBoolean = filterBy(arrEx, 'boolean')
const arrByFilterString = filterBy(arrEx, 'string')
const arrByFilterNumber = filterBy(arrEx, 'number')
const arrByFilterObject = filterBy(arrEx, 'object')

console.log(arrByFilterBoolean)
console.log(arrByFilterString)
console.log(arrByFilterNumber)
console.log(arrByFilterObject)
console.log(arrEx)


